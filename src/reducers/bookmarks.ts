import { BOOKMARK_ELEMENT, UBBOOKMARK_ELEMENT } from "../actions/types";

import { IBookmark } from "../consts";

const initialState: IBookmark[] = [];

const bookmarkReducer = (
  state = initialState,
  action: { type: string; payload: any }
) => {
  const { type, payload } = action;
  switch (type) {
    case BOOKMARK_ELEMENT:
      return [...state, payload];
    case UBBOOKMARK_ELEMENT:
      const bookmarks = state.filter((bookmark) => bookmark.id !== payload);
      return bookmarks;
    default:
      return state;
  }
};

export default bookmarkReducer;
