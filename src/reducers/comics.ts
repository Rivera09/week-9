import { LOAD_COMICS } from "../actions/types";

const initialState: any = {};

function comicsReducers(state = initialState, action: any) {
  const { type, payload } = action;
  switch (type) {
    case LOAD_COMICS:
      return { ...state, ...payload };
    default:
      return state;
  }
}

export default comicsReducers;
