import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

import characters from "./characters";
import comics from "./comics";
import stories from "./stories";
import bookmarks from "./bookmarks";

const persistConfig = {
  key: "root",
  storage,
  whitelist: ["bookmarks"],
};

const rootReducer = combineReducers({
  characters,
  comics,
  stories,
  bookmarks,
});

export default persistReducer(persistConfig, rootReducer);
