import { LOAD_CHARACTERS, LOAD_CHARACTER_DATA } from "../actions/types";

import { ICharactersState } from "../consts";

const initialState: ICharactersState | {} = {};

function charactersReducers(state = initialState, action: any) {
  const { type, payload } = action;
  switch (type) {
    case LOAD_CHARACTERS:
    case LOAD_CHARACTER_DATA:
      return { ...state, ...payload };
    default:
      return state;
  }
}

export default charactersReducers;
