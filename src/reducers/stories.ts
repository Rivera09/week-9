import { LOAD_STORIES } from "../actions/types";

const initialState: any = {};

function storiesReducers(state = initialState, action: any) {
  const { type, payload } = action;
  switch (type) {
    case LOAD_STORIES:
      return { ...state, ...payload };
    default:
      return state;
  }
}

export default storiesReducers;
