export default class Connection {
  constructor() {
    if (!Connection.instance) {
      Connection.instance = {
        async getData(url) {
          try {
            const res = await fetch(url);
            return { resBody: await res.json(), error: res.status !== 200 };
          } catch (e) {
            return { error: true };
          }
        },
      };
    }
    return Connection.instance;
  }
}
