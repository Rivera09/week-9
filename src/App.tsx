import React, { Suspense } from "react";
import { Provider } from "react-redux";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";
import { PersistGate } from "redux-persist/integration/react";

import { Navbar, Footer } from "./componets";
import { store, persistor } from "./store";
import "./sass/styles.scss";

const Home = React.lazy(() => import("./pages/Home"));
const Characters = React.lazy(() => import("./pages/Characters"));
const Comics = React.lazy(() => import("./pages/Comics"));
const Stories = React.lazy(() => import("./pages/Stories"));
const CharacterDetails = React.lazy(() => import("./pages/CharacterDetails"));
const ComicDetails = React.lazy(() => import("./pages/ComicDetails"));
const StoryDetails = React.lazy(() => import("./pages/StoryDetails"));
const Bookmarks = React.lazy(() => import("./pages/Bookmarks"));

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Suspense fallback={<div>Loading</div>}>
          <PersistGate persistor={persistor}>
            <Navbar />
            <Switch>
              <Route path="/" exact component={Home} />
              <Route path="/characters" exact component={Characters} />
              <Route path="/comics" exact component={Comics} />
              <Route
                path="/characters/:id"
                exact
                component={CharacterDetails}
              />
              <Route path="/comics/:id" exact component={ComicDetails} />
              <Route path="/stories/:id" exact component={StoryDetails} />
              <Route path="/bookmarks" exact component={Bookmarks} />
              <Route path="/stories" exact component={Stories} />
            </Switch>
            <Footer />
          </PersistGate>
        </Suspense>
      </Router>
    </Provider>
  );
}

export default App;
