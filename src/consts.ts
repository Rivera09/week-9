const publicApiKey = process.env.REACT_APP_MARVEL_PUBLIC_KEY;
const apiHashCode = process.env.REACT_APP_MARVERL_HASH_CODE;

const marvelApiAuthParams = `ts=1&apikey=${publicApiKey}&hash=${apiHashCode}`;

export const FETCH_CHARACTERS = "FETCH_CHARACTERS";

const BASE_URL = "https://gateway.marvel.com/v1/public";

// url to access characters
export const CHARACTERS_ENDPOINT_URL =
  BASE_URL + "/characters?limit=:limit&" + marvelApiAuthParams;

export const CHARACTERS_NAME_FILTER_ENDPOINT_URL =
  BASE_URL +
  "/characters?nameStartsWith=:name&limit=:limit&" +
  marvelApiAuthParams;

export const CHARACTERS_COMIC_FILTER_ENDPOINT_URL =
  BASE_URL + "/comics/:comicId/characters?" + marvelApiAuthParams;

export const CHARACTERS_STORY_FILTER_ENDPOINT_URL =
  BASE_URL + "/stories/:storyId/characters?" + marvelApiAuthParams;

export const CHARACTER_DETAILS_ENDPOINT_URL =
  BASE_URL + "/characters/:id?" + marvelApiAuthParams;

// url to access comics
export const COMICS_ENDPOINT_URL =
  BASE_URL + "/comics?limit=:limit&" + marvelApiAuthParams;

export const COMICS_NAME_ENDPOINT_URL =
  BASE_URL +
  "/comics?titleStartsWith=:name&limit=:limit&" +
  marvelApiAuthParams;

export const COMIC_DETAILS_ENDPOINT_URL =
  BASE_URL + "/comics/:id?" + marvelApiAuthParams;

export const COMICS_FORMAT_ENDPOINT_URL =
  BASE_URL + "/comics?formatType=:type&limit=:limit&" + marvelApiAuthParams;

// url to access stories

export const STORIES_ENDPOINT_URL =
  BASE_URL + "/stories?limit=:limit&" + marvelApiAuthParams;

export const STORY_DATA_ENDPOINT_URL =
  BASE_URL + "/stories/:id?" + marvelApiAuthParams;

export const PORTRAIT_IMAGE = ":url/portrait_xlarge.:ext";
export const LANDSCAPE_IMAGE = ":url/landscape_incredible.:ext";

//   Props

export interface CharactersProps {
  fetchCharacters: Function;
  fetchComics: Function;
  fetchStories: Function;
  characters: ICharacter[];
  comics: IComic[];
  stories: any[];
  paginationCount: number;
  totalCharacters: number;
}

export interface ComicsProps {
  fetchComics: Function;
  comics: IComic[];
  paginationCount: number;
  totalComics: number;
}

export interface ComicDetailsProps {
  fetchComicData: Function;
  comics: IComic[];
}

export interface StoryDetailsProps {
  fetchStoryDetails: Function;
  stories: any[];
}

export interface HomeProps {
  fetchCharacters: Function;
  fetchComics: Function;
  fetchStories: Function;
  characters: ICharacter[];
  comics: IComic[];
  stories: any[];
}

export interface CharactersFilterFormProps {
  onChange: Function;
  onSubmit: Function;
  comics: any[];
  stories: any[];
}

export interface ComicsFilterFormProps {
  onChange: Function;
  onSubmit: Function;
}

// Interfaces

export interface IBookmark {
  id: string | number;
  cardCover: string;
  cardTitle: string;
  type: string;
}

export interface IPagination {
  offset: number;
  limit: number;
  total: number;
  count: number;
}

export interface ICharactersState {
  offset: number;
  limit: number;
  total: number;
  count: number;
  results: ICharacter[];
}

export interface IComic {
  id: number;
  digitalId: number;
  title: string;
  issueNumber: number;
  variantDescription: string;
  description: string | null;
  modified: string;
  isbn: string;
  upc: string;
  diamondCode: string;
  ean: string;
  issn: string;
  format: string;
  pageCount: number;
  series: CollectionItem;
  variants: any[];
  collections: any[];
  collectedIssues: any[];
  dates: DateElement[];
  prices: Price[];
  thumbnail: Thumbnail;
  images: Thumbnail[];
  creators: ComicCharacters;
  characters: ComicCharacters;
  stories: ComicCharacters;
  events: ComicCharacters;
}

export interface ICharacter {
  id: number;
  name: string;
  description: string;
  modified: string;
  thumbnail: Thumbnail;
  resourceURI: string;
  comics: CollectionData;
  series: CollectionData;
  stories: CollectionData;
  events: CollectionData;
  urls: URL[];
}

interface DateElement {
  type: string;
  date: string;
}

interface Price {
  type: string;
  price: number;
}

export interface ComicCharacters {
  available: number;
  collectionURI: string;
  items: Item[];
  returned: number;
}

export interface Item {
  resourceURI: string;
  name: string;
  role?: string;
  type?: string;
}

interface CollectionData {
  available: number;
  collectionURI: string;
  items: CollectionItem[];
  returned: number;
}

interface CollectionItem {
  resourceURI: string;
  name: string;
  type?: Type;
}

// interface TextObject {
//   type: string;
//   language: string;
//   text: string;
// }

enum Type {
  Cover = "cover",
  InteriorStory = "interiorStory",
}

interface Thumbnail {
  path: string;
  extension: string;
}

interface URL {
  type: string;
  url: string;
}
