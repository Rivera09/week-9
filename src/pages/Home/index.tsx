import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import { Card, Header } from "../../componets";
import { fetchCharacters, fetchComics, fetchStories } from "../../actions";
import { HomeProps } from "../../consts";
import { PORTRAIT_IMAGE } from "../../consts";

const Home: React.FC<HomeProps> = ({
  fetchCharacters,
  fetchComics,
  fetchStories,
  stories,
  characters,
  comics,
}) => {
  useEffect(() => {
    const fetchData = async () => {
      fetchCharacters(4);
      fetchComics(4);
      fetchStories(4);
    };
    fetchData();
  }, [fetchCharacters, fetchComics, fetchStories]);

  return (
    <>
      <Header title="Home" />
      <div className="page-container">
        <h2>
          <Link to="/characters">Characters &#8594;</Link>
        </h2>
        <div className="grid-cards-container">
          {characters?.map((character: any) => (
            <Card
              cardTitle={character.name}
              id={character.id}
              key={character.id}
              linkTo={`/characters/${character.id}`}
              type="characters"
              cardCover={
                character.thumbnail?.path
                  ? PORTRAIT_IMAGE.replace(
                      ":url",
                      character.thumbnail?.path
                    ).replace(":ext", character.thumbnail?.extension)
                  : null
              }
            />
          ))}
        </div>
        <h2>
          <Link to="/comics">Comics &#8594;</Link>
        </h2>
        <div className="grid-cards-container">
          {comics?.map((comic: any) => (
            <Card
              cardTitle={comic.title}
              id={comic.id}
              linkTo={`/comics/${comic.id}`}
              key={comic.id}
              type="comics"
              cardCover={
                comic.thumbnail?.path
                  ? PORTRAIT_IMAGE.replace(
                      ":url",
                      comic.thumbnail?.path
                    ).replace(":ext", comic.thumbnail?.extension)
                  : null
              }
            />
          ))}
        </div>
        <h2>
          <Link to="/stories">Stories &#8594;</Link>
        </h2>
        <div className="grid-cards-container">
          {stories?.map((story) => (
            <Card
              key={story.id}
              cardTitle={story.title}
              linkTo={`/stories/${story.id}`}
              type="stories"
              cardCover={
                story.thumbnail?.path
                  ? PORTRAIT_IMAGE.replace(
                      ":url",
                      story.thumbnail?.path
                    ).replace(":ext", story.thumbnail?.extension)
                  : null
              }
            />
          ))}
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state: any) => ({
  characters: state.characters.results,
  comics: state.comics.results,
  stories: state.stories.results,
});

export default connect(mapStateToProps, {
  fetchCharacters,
  fetchComics,
  fetchStories,
})(Home);
