import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import { rest } from "msw";
import { setupServer } from "msw/node";
import { render, fireEvent, waitForDomChange } from "@testing-library/react";
import "@testing-library/jest-dom";

import Characters from "./index";
import { store } from "../../store";
import {
  charactersResponse,
  spiderResponse,
  // comicCharacters,
} from "../../mockedData";

const server = setupServer(
  rest.get(
    "https://gateway.marvel.com/v1/public/characters",
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(charactersResponse));
    }
  )
);

beforeAll(() => server.listen());
afterAll(() => server.close());
afterEach(() => server.resetHandlers());

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <Provider store={store}>
      <Router>
        <Characters />
      </Router>
    </Provider>,
    div
  );
});

it("search input works", async () => {
  const { getByPlaceholderText, getByText } = render(
    <Provider store={store}>
      <Router>
        <Characters />
      </Router>
    </Provider>
  );

  server.use(
    rest.get(
      "https://gateway.marvel.com/v1/public/characters",
      (req, res, ctx) => {
        return res(ctx.status(200), ctx.json(spiderResponse));
      }
    )
  );

  const searchInput = getByPlaceholderText("character name");
  const submitBtn = getByText("Buscar");
  // const selectInput = getByText("Select a comic");
  fireEvent.change(searchInput, { target: { value: "spider" } });
  fireEvent.click(submitBtn);
  await waitForDomChange();
  getByText("Spider-dok");

  // server.use(
  //   rest.get(
  //     "https://gateway.marvel.com/v1/public/comics",
  //     (req, res, ctx) => {
  //       return res(ctx.status(200), ctx.json(comicCharacters));
  //     }
  //   )
  // );

  // fireEvent.change(searchInput, { target: { value: "" } });
  // fireEvent.change(selectInput, {
  //   target: {
  //     value: "1158",
  //   },
  // });
  // fireEvent.click(submitBtn);
  // await waitForDomChange();

  // debug();
});
