import React, { useEffect, useState, useCallback } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { connect } from "react-redux";

import { Card, Header, CharactersFilterForm } from "../../componets";
import { fetchCharacters, fetchComics, fetchStories } from "../../actions";
import { ICharacter, PORTRAIT_IMAGE, CharactersProps } from "../../consts";

const Characters: React.FC<CharactersProps> = ({
  fetchCharacters,
  characters,
  fetchComics,
  fetchStories,
  paginationCount,
  totalCharacters,
  comics,
  stories,
}) => {
  const [filterFormData, setFilterFormData] = useState({
    characterName: "",
    comicId: "",
    storyId: "",
  });
  const [limit, setLimit] = useState(20);

  const history = useHistory();
  const location = useLocation();

  const fetchData = useCallback(() => {
    const queryParams = new URLSearchParams(location.search);
    const name = queryParams.get("name");
    const comicId = queryParams.get("comic");
    if (name) fetchCharacters(limit, "name", name);
    else if (comicId) fetchCharacters(limit, "comic", comicId);
    else fetchCharacters(limit);
    fetchComics(30);
    fetchStories(30);
  }, [fetchCharacters, fetchComics, fetchStories, location.search, limit]);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  useEffect(() => {
    const checkIfBottom = () => {
      if (
        window.innerHeight + window.scrollY >= document.body.offsetHeight &&
        paginationCount < totalCharacters
      ) {
        setLimit((prevLimit: number) => prevLimit + 20);
      }
    };
    window.addEventListener("scroll", checkIfBottom);
    return () => window.removeEventListener("scroll", checkIfBottom);
  }, [totalCharacters]);

  const handleFormChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFilterFormData({ ...filterFormData, [e.target.name]: e.target.value });
  };

  const handleFormSubmit = (e: React.ChangeEvent<HTMLInputElement>) => {
    setLimit(20);
    e.preventDefault();
    if (filterFormData.characterName) {
      history.push(
        `/characters?name=${filterFormData.characterName.replace(" ", "%20")}`
      );
      fetchCharacters(
        20,
        "name",
        filterFormData.characterName.replace(" ", "%20")
      );
    } else if (filterFormData.comicId) {
      history.push(`/characters?comic=${filterFormData.comicId}`);
      fetchCharacters(20, "comic", filterFormData.comicId);
    } else if (filterFormData.storyId) {
      history.push(`/characters?story=${filterFormData.storyId}`);
      fetchCharacters(20, "story", filterFormData.storyId);
    } else {
      history.push("/characters");
      fetchCharacters(20);
    }
  };

  return (
    <>
      <Header title="Characters" />
      <div className="page-container">
        <CharactersFilterForm
          onChange={handleFormChange}
          onSubmit={handleFormSubmit}
          comics={comics}
          stories={stories}
        />
        <div className="grid-cards-container">
          {characters?.map((character: ICharacter) => (
            <Card
              cardTitle={character.name}
              id={character.id}
              linkTo={`/characters/${character.id}`}
              key={character.id}
              type="characters"
              cardCover={
                character.thumbnail?.path
                  ? PORTRAIT_IMAGE.replace(
                      ":url",
                      character.thumbnail?.path
                    ).replace(":ext", character.thumbnail?.extension)
                  : null
              }
            />
          ))}
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state: any) => ({
  characters: state?.characters?.results,
  comics: state?.comics?.results,
  stories: state?.stories?.results,
  paginationCount: state?.characters?.count,
  totalCharacters: state?.characters?.total,
});

export default connect(mapStateToProps, {
  fetchCharacters,
  fetchComics,
  fetchStories,
})(Characters);
