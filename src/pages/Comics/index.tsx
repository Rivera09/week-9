import React, { useEffect, useCallback, useState } from "react";
import { useHistory } from "react-router-dom";
import { connect } from "react-redux";

import { Header, Card, ComicsFilterForm } from "../../componets";
import { fetchComics } from "../../actions";
import { ComicsProps, PORTRAIT_IMAGE } from "../../consts";

const Comics: React.FC<ComicsProps> = ({
  fetchComics,
  comics,
  paginationCount,
  totalComics,
}) => {
  const [filterFormData, setFilterFormData] = useState({
    comicName: "",
    comicFormatType: "",
  });
  const [limit, setLimit] = useState(20);

  const history = useHistory();
  // const location = useLocation();

  const fetchData = useCallback(() => {
    fetchComics(limit);
  }, [fetchComics, limit]);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  useEffect(() => {
    const checkIfBottom = () => {
      if (
        window.innerHeight + window.scrollY >= document.body.offsetHeight &&
        paginationCount < totalComics
      ) {
        setLimit((prevLimit: number) => prevLimit + 20);
      }
    };
    window.addEventListener("scroll", checkIfBottom);
    return () => window.removeEventListener("scroll", checkIfBottom);
  }, [totalComics]);

  const handleFormChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFilterFormData({ ...filterFormData, [e.target.name]: e.target.value });
  };

  const handleFormSubmit = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    setLimit(20);
    if (filterFormData.comicName) {
      history.push(
        `/comics?title=${filterFormData.comicName.replace(" ", "%20")}`
      );
      fetchComics(20, "name", filterFormData.comicName);
    } else if (filterFormData.comicFormatType) {
      history.push(`/comics?format=${filterFormData.comicFormatType}`);
      fetchComics(20, "format", filterFormData.comicFormatType);
    } else {
      history.push("/comics");
    }
  };

  return (
    <>
      <Header title="Comic" />
      <div className="page-container">
        <ComicsFilterForm
          onChange={handleFormChange}
          onSubmit={handleFormSubmit}
        />
        <div className="grid-cards-container">
          {comics?.map((comic) => (
            <Card
              cardTitle={comic.title}
              id={comic.id}
              linkTo={`/comics/${comic.id}`}
              key={comic.id}
              type="comics"
              cardCover={
                comic.thumbnail?.path
                  ? PORTRAIT_IMAGE.replace(
                      ":url",
                      comic.thumbnail?.path
                    ).replace(":ext", comic.thumbnail?.extension)
                  : null
              }
            />
          ))}
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state: any) => ({
  comics: state?.comics?.results,
  paginationCount: state?.comics?.count,
  totalComics: state?.comics?.total,
});

export default connect(mapStateToProps, { fetchComics })(Comics);
