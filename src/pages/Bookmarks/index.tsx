import React from "react";
import { connect } from "react-redux";

import { Card, Header } from "../../componets";
import { IBookmark } from "../../consts";

const Home: React.FC<{ bookmarks: IBookmark[] }> = ({ bookmarks }) => {
  return (
    <>
      <Header title="Bookmarks" />
      <div className="page-container">
        <div className="grid-cards-container">
          {bookmarks?.map((element: any) => (
            <Card
              cardTitle={element.cardTitle}
              id={element.id}
              key={element.id}
              linkTo={`/${element.type}/${element.id}`}
              cardCover={element.cardCover}
            />
          ))}
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state: any) => ({
  bookmarks: state?.bookmarks,
});

export default connect(mapStateToProps, {})(Home);
