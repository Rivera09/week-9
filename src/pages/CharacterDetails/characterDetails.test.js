import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import { rest } from "msw";
import { setupServer } from "msw/node";
import { render, waitForDomChange } from "@testing-library/react";
import "@testing-library/jest-dom";

import CharacterDetails from "./index";
import { store } from "../../store";
import { characterDetails } from "../../mockedData";

const server = setupServer(
  rest.get(
    "https://gateway.marvel.com/v1/public/characters/0",
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(characterDetails));
    }
  )
);

beforeAll(() => server.listen());
afterAll(() => server.close());
afterEach(() => server.resetHandlers());

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <Provider store={store}>
      <Router>
        <CharacterDetails />
      </Router>
    </Provider>,
    div
  );
});

it("character details load correctly", async () => {
  const { getByText } = render(
    <Provider store={store}>
      <Router>
        <CharacterDetails />
      </Router>
    </Provider>
  );

  await waitForDomChange();
  getByText("Dead Girl");
  // await findByText("Dead Girl");
  // debug();
});
