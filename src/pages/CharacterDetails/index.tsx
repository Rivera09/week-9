import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { connect } from "react-redux";

import { Header } from "../../componets";
import { fetchCharacterData } from "../../actions";
import { ICharacter, LANDSCAPE_IMAGE } from "../../consts";

const CharacterDetails: React.FC<{
  fetchCharacterData: Function;
  characters: ICharacter[];
}> = ({ fetchCharacterData, characters }) => {
  const { id } = useParams<{ id?: string }>();
  useEffect(() => {
    fetchCharacterData(id || "0");
  }, [fetchCharacterData, id]);
  const characterData = characters ? characters[0] : null;
  return (
    <>
      <Header
        title={characterData?.name || "Character details"}
        image={
          characterData?.thumbnail
            ? LANDSCAPE_IMAGE.replace(
                ":url",
                characterData?.thumbnail.path
              ).replace(":ext", characterData.thumbnail.extension)
            : null
        }
      />
      <div className="page-container">
        <p className="character-description">
          {characterData?.description || "No description available"}
        </p>

        <h2>Comics</h2>
        <ul className="details-list">
          {characterData?.comics.items.map((item) => (
            <li key={item.name}>{item.name}</li>
          ))}
        </ul>
        <h2>Stories</h2>
        <ul className="details-list">
          {characterData?.stories.items.map((item) => (
            <li key={item.name}>{item.name}</li>
          ))}
        </ul>
      </div>
    </>
  );
};

const mapStateToProps = (state: any) => ({
  characters: state?.characters?.results,
});

export default connect(mapStateToProps, { fetchCharacterData })(
  CharacterDetails
);
