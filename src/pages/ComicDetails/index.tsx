import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { connect } from "react-redux";

import { Header } from "../../componets";
import { fetchComicData } from "../../actions";
import { ComicDetailsProps, LANDSCAPE_IMAGE } from "../../consts";

const ComicDetails: React.FC<ComicDetailsProps> = ({
  comics,
  fetchComicData,
}) => {
  const { id } = useParams<{ id?: string }>();
  useEffect(() => {
    fetchComicData(id || "0");
  }, [fetchComicData, id]);
  const comicData = comics ? comics[0] : null;

  return (
    <>
      <Header
        title={comicData?.title || "Comic details"}
        image={
          comicData?.thumbnail
            ? LANDSCAPE_IMAGE.replace(
                ":url",
                comicData?.thumbnail.path
              ).replace(":ext", comicData.thumbnail.extension)
            : null
        }
      />
      <div className="page-container">
        <p className="character-description">
          {comicData?.description || "No description available"}
        </p>
        <h2>Characters</h2>
        <ul className="details-list">
          {comicData?.characters.items.map((item) => (
            <li key={item.name}>{item.name}</li>
          ))}
        </ul>
        <h2>Stories</h2>
        <ul className="details-list">
          {comicData?.stories.items.map((item) => (
            <li key={item.name}>{item.name}</li>
          ))}
        </ul>
        {/* <h2>Comics where character appears</h2>
        <div className="grid-cards-container">
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
        </div>
        <h2>Stories where character appears</h2>
        <div className="grid-cards-container">
          <Card />
          <Card />
          <Card />
          <Card />
        </div> */}
      </div>
    </>
  );
};

const mapStateToProps = (state: any) => ({
  comics: state?.comics?.results,
});

export default connect(mapStateToProps, { fetchComicData })(ComicDetails);
