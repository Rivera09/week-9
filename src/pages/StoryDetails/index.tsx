import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { connect } from "react-redux";

import { Header } from "../../componets";
import { fetchStoryDetails } from "../../actions";
import { StoryDetailsProps, LANDSCAPE_IMAGE } from "../../consts";

const StoryDetails: React.FC<StoryDetailsProps> = ({
  stories,
  fetchStoryDetails,
}) => {
  const { id } = useParams<{ id?: string }>();
  useEffect(() => {
    fetchStoryDetails(id || "0");
  }, [fetchStoryDetails, id]);
  const storyData = stories ? stories[0] : null;

  return (
    <>
      <Header
        title={storyData?.title || "Story details"}
        image={
          storyData?.thumbnail
            ? LANDSCAPE_IMAGE.replace(
                ":url",
                storyData?.thumbnail.path
              ).replace(":ext", storyData.thumbnail.extension)
            : null
        }
      />
      <div className="page-container">
        <p className="character-description">
          {storyData?.description || "No description available"}
        </p>

        <h2>Characters</h2>
        <ul className="details-list">
          {storyData?.characters.items.map((item: any) => (
            <li key={item.name}>{item.name}</li>
          ))}
        </ul>
        <h2>Comics</h2>
        <ul className="details-list">
          {storyData?.comics?.items.map((item: any) => (
            <li key={item.name}>{item.name}</li>
          ))}
        </ul>
      </div>
    </>
  );
};

const mapStateToProps = (state: any) => ({
  stories: state?.stories?.results,
});

export default connect(mapStateToProps, { fetchStoryDetails })(StoryDetails);
