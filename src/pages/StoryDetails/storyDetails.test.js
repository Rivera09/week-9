import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import { rest } from "msw";
import { setupServer } from "msw/node";
import { render, waitForDomChange } from "@testing-library/react";
import "@testing-library/jest-dom";

import StoryDetails from "./index";
import { store } from "../../store";
import { storyDetails } from "../../mockedData";

const server = setupServer(
  rest.get(
    "https://gateway.marvel.com/v1/public/stories/0",
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(storyDetails));
    }
  )
);

beforeAll(() => server.listen());
afterAll(() => server.close());
afterEach(() => server.resetHandlers());

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <Provider store={store}>
      <Router>
        <StoryDetails />
      </Router>
    </Provider>,
    div
  );
});

it("fetches data correctly", async () => {
  const { getByText } = render(
    <Provider store={store}>
      <Router>
        <StoryDetails />
      </Router>
    </Provider>
  );

  await waitForDomChange();
  getByText(
    "Investigating the murder of a teenage girl, Cage suddenly learns that a three-way gang war is under way for control of the turf"
  );
});
