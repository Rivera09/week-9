import React, { useEffect, useCallback, useState } from "react";
import { connect } from "react-redux";

import { Header, Card } from "../../componets";
import { fetchStories } from "../../actions";
import { PORTRAIT_IMAGE } from "../../consts";

const Characters: React.FC<{
  fetchStories: Function;
  stories: any[];
  paginationCount: number;
  totalStories: number;
}> = ({ fetchStories, stories, paginationCount, totalStories }) => {
  const [limit, setLimit] = useState(20);
  const fetchData = useCallback(() => {
    fetchStories(limit);
  }, [fetchStories, limit]);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  useEffect(() => {
    const checkIfBottom = () => {
      if (
        window.innerHeight + window.scrollY >= document.body.offsetHeight &&
        paginationCount < totalStories
      ) {
        setLimit((prevLimit: number) => prevLimit + 20);
      }
    };
    window.addEventListener("scroll", checkIfBottom);
    return () => window.removeEventListener("scroll", checkIfBottom);
  }, [totalStories]);

  return (
    <>
      <Header title="Stories" />
      <div className="page-container">
        <div className="grid-cards-container">
          {stories?.map((story) => (
            <Card
              cardTitle={story.title}
              id={story.id}
              linkTo={`/stories/${story.id}`}
              key={story.id}
              type="stories"
              cardCover={
                story.thumbnail?.path
                  ? PORTRAIT_IMAGE.replace(
                      ":url",
                      story.thumbnail?.path
                    ).replace(":ext", story.thumbnail?.extension)
                  : null
              }
            />
          ))}
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state: any) => ({
  stories: state?.stories?.results,
  paginationCount: state?.stories?.count,
  totalStories: state?.stories?.total,
});

export default connect(mapStateToProps, { fetchStories })(Characters);
