export const charactersResponse = {
  code: 200,
  status: "Ok",
  copyright: "© 2021 MARVEL",
  attributionText: "Data provided by Marvel. © 2021 MARVEL",
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2021 MARVEL</a>',
  etag: "fb6cbf9d33e42c05b846cf1434ed2fe0a353c8d9",
  data: {
    offset: 0,
    limit: 1,
    total: 1493,
    count: 1,
    results: [
      {
        id: 1011334,
        name: "3-D Man",
        description: "",
        modified: "2014-04-29T14:18:17-0400",
        thumbnail: {
          path: "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784",
          extension: "jpg",
        },
        resourceURI: "http://gateway.marvel.com/v1/public/characters/1011334",
        comics: {
          available: 12,
          collectionURI:
            "http://gateway.marvel.com/v1/public/characters/1011334/comics",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/21366",
              name: "Avengers: The Initiative (2007) #14",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/24571",
              name: "Avengers: The Initiative (2007) #14 (SPOTLIGHT VARIANT)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/21546",
              name: "Avengers: The Initiative (2007) #15",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/21741",
              name: "Avengers: The Initiative (2007) #16",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/21975",
              name: "Avengers: The Initiative (2007) #17",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/22299",
              name: "Avengers: The Initiative (2007) #18",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/22300",
              name: "Avengers: The Initiative (2007) #18 (ZOMBIE VARIANT)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/22506",
              name: "Avengers: The Initiative (2007) #19",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/8500",
              name: "Deadpool (1997) #44",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/10223",
              name: "Marvel Premiere (1972) #35",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/10224",
              name: "Marvel Premiere (1972) #36",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/10225",
              name: "Marvel Premiere (1972) #37",
            },
          ],
          returned: 12,
        },
        series: {
          available: 3,
          collectionURI:
            "http://gateway.marvel.com/v1/public/characters/1011334/series",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/1945",
              name: "Avengers: The Initiative (2007 - 2010)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/2005",
              name: "Deadpool (1997 - 2002)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/2045",
              name: "Marvel Premiere (1972 - 1981)",
            },
          ],
          returned: 3,
        },
        stories: {
          available: 21,
          collectionURI:
            "http://gateway.marvel.com/v1/public/characters/1011334/stories",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/19947",
              name: "Cover #19947",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/19948",
              name: "The 3-D Man!",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/19949",
              name: "Cover #19949",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/19950",
              name: "The Devil's Music!",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/19951",
              name: "Cover #19951",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/19952",
              name: "Code-Name:  The Cold Warrior!",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/47184",
              name: "AVENGERS: THE INITIATIVE (2007) #14",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/47185",
              name: "Avengers: The Initiative (2007) #14 - Int",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/47498",
              name: "AVENGERS: THE INITIATIVE (2007) #15",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/47499",
              name: "Avengers: The Initiative (2007) #15 - Int",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/47792",
              name: "AVENGERS: THE INITIATIVE (2007) #16",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/47793",
              name: "Avengers: The Initiative (2007) #16 - Int",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/48361",
              name: "AVENGERS: THE INITIATIVE (2007) #17",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/48362",
              name: "Avengers: The Initiative (2007) #17 - Int",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/49103",
              name: "AVENGERS: THE INITIATIVE (2007) #18",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/49104",
              name: "Avengers: The Initiative (2007) #18 - Int",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/49106",
              name: "Avengers: The Initiative (2007) #18, Zombie Variant - Int",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/49888",
              name: "AVENGERS: THE INITIATIVE (2007) #19",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/49889",
              name: "Avengers: The Initiative (2007) #19 - Int",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/54371",
              name:
                "Avengers: The Initiative (2007) #14, Spotlight Variant - Int",
              type: "interiorStory",
            },
          ],
          returned: 20,
        },
        events: {
          available: 1,
          collectionURI:
            "http://gateway.marvel.com/v1/public/characters/1011334/events",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/events/269",
              name: "Secret Invasion",
            },
          ],
          returned: 1,
        },
        urls: [
          {
            type: "detail",
            url:
              "http://marvel.com/characters/74/3-d_man?utm_campaign=apiRef&utm_source=a6bc452661a7d43e08825c3f95534305",
          },
          {
            type: "wiki",
            url:
              "http://marvel.com/universe/3-D_Man_(Chandler)?utm_campaign=apiRef&utm_source=a6bc452661a7d43e08825c3f95534305",
          },
          {
            type: "comiclink",
            url:
              "http://marvel.com/comics/characters/1011334/3-d_man?utm_campaign=apiRef&utm_source=a6bc452661a7d43e08825c3f95534305",
          },
        ],
      },
    ],
  },
};

export const spiderResponse = {
  code: 200,
  status: "Ok",
  copyright: "© 2021 MARVEL",
  attributionText: "Data provided by Marvel. © 2021 MARVEL",
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2021 MARVEL</a>',
  etag: "fb6cbf9d33e42c05b846cf1434ed2fe0a353c8d9",
  data: {
    offset: 0,
    limit: 1,
    total: 1493,
    count: 1,
    results: [
      {
        id: 1011334,
        name: "Spider-dok",
        description: "",
        modified: "2014-04-29T14:18:17-0400",
        thumbnail: {
          path: "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784",
          extension: "jpg",
        },
        resourceURI: "http://gateway.marvel.com/v1/public/characters/1011334",
        comics: {
          available: 12,
          collectionURI:
            "http://gateway.marvel.com/v1/public/characters/1011334/comics",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/21366",
              name: "Avengers: The Initiative (2007) #14",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/24571",
              name: "Avengers: The Initiative (2007) #14 (SPOTLIGHT VARIANT)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/21546",
              name: "Avengers: The Initiative (2007) #15",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/21741",
              name: "Avengers: The Initiative (2007) #16",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/21975",
              name: "Avengers: The Initiative (2007) #17",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/22299",
              name: "Avengers: The Initiative (2007) #18",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/22300",
              name: "Avengers: The Initiative (2007) #18 (ZOMBIE VARIANT)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/22506",
              name: "Avengers: The Initiative (2007) #19",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/8500",
              name: "Deadpool (1997) #44",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/10223",
              name: "Marvel Premiere (1972) #35",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/10224",
              name: "Marvel Premiere (1972) #36",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/10225",
              name: "Marvel Premiere (1972) #37",
            },
          ],
          returned: 12,
        },
        series: {
          available: 3,
          collectionURI:
            "http://gateway.marvel.com/v1/public/characters/1011334/series",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/1945",
              name: "Avengers: The Initiative (2007 - 2010)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/2005",
              name: "Deadpool (1997 - 2002)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/2045",
              name: "Marvel Premiere (1972 - 1981)",
            },
          ],
          returned: 3,
        },
        stories: {
          available: 21,
          collectionURI:
            "http://gateway.marvel.com/v1/public/characters/1011334/stories",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/19947",
              name: "Cover #19947",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/19948",
              name: "The 3-D Man!",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/19949",
              name: "Cover #19949",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/19950",
              name: "The Devil's Music!",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/19951",
              name: "Cover #19951",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/19952",
              name: "Code-Name:  The Cold Warrior!",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/47184",
              name: "AVENGERS: THE INITIATIVE (2007) #14",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/47185",
              name: "Avengers: The Initiative (2007) #14 - Int",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/47498",
              name: "AVENGERS: THE INITIATIVE (2007) #15",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/47499",
              name: "Avengers: The Initiative (2007) #15 - Int",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/47792",
              name: "AVENGERS: THE INITIATIVE (2007) #16",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/47793",
              name: "Avengers: The Initiative (2007) #16 - Int",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/48361",
              name: "AVENGERS: THE INITIATIVE (2007) #17",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/48362",
              name: "Avengers: The Initiative (2007) #17 - Int",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/49103",
              name: "AVENGERS: THE INITIATIVE (2007) #18",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/49104",
              name: "Avengers: The Initiative (2007) #18 - Int",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/49106",
              name: "Avengers: The Initiative (2007) #18, Zombie Variant - Int",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/49888",
              name: "AVENGERS: THE INITIATIVE (2007) #19",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/49889",
              name: "Avengers: The Initiative (2007) #19 - Int",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/54371",
              name:
                "Avengers: The Initiative (2007) #14, Spotlight Variant - Int",
              type: "interiorStory",
            },
          ],
          returned: 20,
        },
        events: {
          available: 1,
          collectionURI:
            "http://gateway.marvel.com/v1/public/characters/1011334/events",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/events/269",
              name: "Secret Invasion",
            },
          ],
          returned: 1,
        },
        urls: [
          {
            type: "detail",
            url:
              "http://marvel.com/characters/74/3-d_man?utm_campaign=apiRef&utm_source=a6bc452661a7d43e08825c3f95534305",
          },
          {
            type: "wiki",
            url:
              "http://marvel.com/universe/3-D_Man_(Chandler)?utm_campaign=apiRef&utm_source=a6bc452661a7d43e08825c3f95534305",
          },
          {
            type: "comiclink",
            url:
              "http://marvel.com/comics/characters/1011334/3-d_man?utm_campaign=apiRef&utm_source=a6bc452661a7d43e08825c3f95534305",
          },
        ],
      },
    ],
  },
};

export const comicCharacters = {
  code: 200,
  status: "Ok",
  copyright: "© 2021 MARVEL",
  attributionText: "Data provided by Marvel. © 2021 MARVEL",
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2021 MARVEL</a>',
  etag: "8205ce462a0645744fb511e82129d7794d6d649a",
  data: {
    offset: 0,
    limit: 1,
    total: 17,
    count: 1,
    results: [
      {
        id: 1010908,
        name: "Beast (Ultimate)",
        description: "",
        modified: "2014-03-05T13:19:55-0500",
        thumbnail: {
          path: "http://i.annihil.us/u/prod/marvel/i/mg/5/d0/53176a9be110c",
          extension: "jpg",
        },
        resourceURI: "http://gateway.marvel.com/v1/public/characters/1010908",
        comics: {
          available: 49,
          collectionURI:
            "http://gateway.marvel.com/v1/public/characters/1010908/comics",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/15770",
              name: "Ultimate Marvel Team-Up (2001) #11",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/5132",
              name:
                "Ultimate Marvel Team-Up Ultimate Collection (Trade Paperback)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/4045",
              name: "Ultimate Spider-Man (2000) #93",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/4746",
              name: "Ultimate Spider-Man Vol. 16: Deadpool (Trade Paperback)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/18475",
              name: "Ultimate War (2003) #2",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/18477",
              name: "Ultimate War (2003) #4",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/15699",
              name: "Ultimate X-Men (2001) #1",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/15710",
              name: "Ultimate X-Men (2001) #2",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/15721",
              name: "Ultimate X-Men (2001) #3",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/15732",
              name: "Ultimate X-Men (2001) #4",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/15743",
              name: "Ultimate X-Men (2001) #5",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/15754",
              name: "Ultimate X-Men (2001) #6",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/15765",
              name: "Ultimate X-Men (2001) #7",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/15767",
              name: "Ultimate X-Men (2001) #8",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/15768",
              name: "Ultimate X-Men (2001) #9",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/15700",
              name: "Ultimate X-Men (2001) #10",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/15701",
              name: "Ultimate X-Men (2001) #11",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/15702",
              name: "Ultimate X-Men (2001) #12",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/15705",
              name: "Ultimate X-Men (2001) #15",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/15706",
              name: "Ultimate X-Men (2001) #16",
            },
          ],
          returned: 20,
        },
        series: {
          available: 10,
          collectionURI:
            "http://gateway.marvel.com/v1/public/characters/1010908/series",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/2311",
              name: "Ultimate Marvel Team-Up (2001 - 2002)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/1823",
              name: "Ultimate Marvel Team-Up Ultimate Collection (2006)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/466",
              name: "Ultimate Spider-Man (2000 - 2009)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/1618",
              name: "Ultimate Spider-Man Vol. 16: Deadpool (2006)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/3659",
              name: "Ultimate War (2003)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/474",
              name: "Ultimate X-Men (2001 - 2009)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/13887",
              name: "Ultimate X-Men MGC (2011)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/1168",
              name: "ULTIMATE X-MEN VOL. 3: WORLD TOUR TPB (2005)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/216",
              name: "ULTIMATE X-MEN VOL. 5: ULTIMATE WAR TPB (1999)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/82",
              name: "Ultimate X-Men Vol. III: World Tour (2002)",
            },
          ],
          returned: 10,
        },
        stories: {
          available: 90,
          collectionURI:
            "http://gateway.marvel.com/v1/public/characters/1010908/stories",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/1380",
              name: "ULTIMATE SPIDER-MAN (2000) #93",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/1886",
              name: "Ultimate X-Men (2001) #39",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/1887",
              name: "Interior #1887",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/2041",
              name: "Ultimate X-Men (2001) #40",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/8105",
              name: "1 of 3 - All-Different",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/31881",
              name: "Previously In Ultimate X-Men:",
              type: "recap",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/31885",
              name: "Ultimate X-Men (2001) #25",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/31886",
              name: "Previously In Ultimate X-Men:",
              type: "recap",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/31887",
              name: "Hellfire and Brimstone Part 5",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/31896",
              name: "Ultimate X-Men (2001) #24",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/31897",
              name: "Previously In Ultimate X-Men:",
              type: "recap",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/31898",
              name: "Hellfire and Brimstone Part 4",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/31901",
              name: "Ultimate X-Men (2001) #23",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/31902",
              name: "Previously In Ultimate X-Men:",
              type: "recap",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/31903",
              name: "Hellfire and Brimstone Part 3",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/32038",
              name: "Ultimate X-Men (2001) #22",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/32039",
              name: "Previously In Ultimate X-Men:",
              type: "recap",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/32040",
              name: "Hellfire and Brimstone Part 2",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/32043",
              name: "Ultimate X-Men (2001) #21",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/32044",
              name: "Previously In Ultimate X-Men:",
              type: "recap",
            },
          ],
          returned: 20,
        },
        events: {
          available: 0,
          collectionURI:
            "http://gateway.marvel.com/v1/public/characters/1010908/events",
          items: [],
          returned: 0,
        },
        urls: [
          {
            type: "detail",
            url:
              "http://marvel.com/characters/3/beast?utm_campaign=apiRef&utm_source=a6bc452661a7d43e08825c3f95534305",
          },
          {
            type: "wiki",
            url:
              "http://marvel.com/universe/Beast_(Ultimate)?utm_campaign=apiRef&utm_source=a6bc452661a7d43e08825c3f95534305",
          },
          {
            type: "comiclink",
            url:
              "http://marvel.com/comics/characters/1010908/beast_ultimate?utm_campaign=apiRef&utm_source=a6bc452661a7d43e08825c3f95534305",
          },
        ],
      },
    ],
  },
};

export const characterDetails = {
  code: 200,
  status: "Ok",
  copyright: "© 2021 MARVEL",
  attributionText: "Data provided by Marvel. © 2021 MARVEL",
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2021 MARVEL</a>',
  etag: "a50d2af98487c9e076d0a656a8ea9eaed64a3206",
  data: {
    offset: 0,
    limit: 20,
    total: 1,
    count: 1,
    results: [
      {
        id: 1011087,
        name: "Dead Girl",
        description: "",
        modified: "1969-12-31T19:00:00-0500",
        thumbnail: {
          path: "http://i.annihil.us/u/prod/marvel/i/mg/5/20/4c0030c61eb15",
          extension: "jpg",
        },
        resourceURI: "http://gateway.marvel.com/v1/public/characters/1011087",
        comics: {
          available: 9,
          collectionURI:
            "http://gateway.marvel.com/v1/public/characters/1011087/comics",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/17971",
              name: "X-Force (1991) #125",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/17972",
              name: "X-Force (1991) #126",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/17973",
              name: "X-Force (1991) #127",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/17974",
              name: "X-Force (1991) #128",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/17975",
              name: "X-Force (1991) #129",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/1028",
              name: "X-Force: Famous, Mutant & Mortal (Hardcover)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/13414",
              name: "X-Statix (2002) #2",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/3433",
              name: "X-Statix Presents: Dead Girl (2006) #1",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/1093",
              name: "X-Statix Vol. I (Trade Paperback)",
            },
          ],
          returned: 9,
        },
        series: {
          available: 5,
          collectionURI:
            "http://gateway.marvel.com/v1/public/characters/1011087/series",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/3633",
              name: "X-Force (1991 - 2004)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/88",
              name: "X-Force: Famous, Mutant & Mortal (2003)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/462",
              name: "X-Statix (2002 - 2004)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/1017",
              name: "X-Statix Presents: Dead Girl (2006)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/153",
              name: "X-Statix Vol. I (2003)",
            },
          ],
          returned: 5,
        },
        stories: {
          available: 9,
          collectionURI:
            "http://gateway.marvel.com/v1/public/characters/1011087/stories",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/5688",
              name: "1 of 5 - A Long Time Dead",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/25972",
              name: "How the Super-Hero Business Works",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/25998",
              name: "[untitled]",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/38220",
              name: "One of Us",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/38221",
              name: "Cover #38221",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/38222",
              name: "As I Die Lying",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/38224",
              name: "Because Louise",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/38226",
              name: "Someone Dies",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/38229",
              name: "X Storm!",
              type: "interiorStory",
            },
          ],
          returned: 9,
        },
        events: {
          available: 0,
          collectionURI:
            "http://gateway.marvel.com/v1/public/characters/1011087/events",
          items: [],
          returned: 0,
        },
        urls: [
          {
            type: "detail",
            url:
              "http://marvel.com/characters/518/dead_girl?utm_campaign=apiRef&utm_source=a6bc452661a7d43e08825c3f95534305",
          },
          {
            type: "wiki",
            url:
              "http://marvel.com/universe/Dead_Girl?utm_campaign=apiRef&utm_source=a6bc452661a7d43e08825c3f95534305",
          },
          {
            type: "comiclink",
            url:
              "http://marvel.com/comics/characters/1011087/dead_girl?utm_campaign=apiRef&utm_source=a6bc452661a7d43e08825c3f95534305",
          },
        ],
      },
    ],
  },
};

export const comicDetails = {
  code: 200,
  status: "Ok",
  copyright: "© 2021 MARVEL",
  attributionText: "Data provided by Marvel. © 2021 MARVEL",
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2021 MARVEL</a>',
  etag: "f712574873e89d0505dc68a908170fb7970d2f13",
  data: {
    offset: 0,
    limit: 20,
    total: 1,
    count: 1,
    results: [
      {
        id: 82967,
        digitalId: 0,
        title: "Marvel Previews (2017)",
        issueNumber: 0,
        variantDescription: "",
        description: null,
        modified: "2019-11-07T08:46:15-0500",
        isbn: "",
        upc: "75960608839302811",
        diamondCode: "",
        ean: "",
        issn: "",
        format: "",
        pageCount: 112,
        textObjects: [],
        resourceURI: "http://gateway.marvel.com/v1/public/comics/82967",
        urls: [
          {
            type: "detail",
            url:
              "http://marvel.com/comics/issue/82967/marvel_previews_2017?utm_campaign=apiRef&utm_source=a6bc452661a7d43e08825c3f95534305",
          },
        ],
        series: {
          resourceURI: "http://gateway.marvel.com/v1/public/series/23665",
          name: "Marvel Previews (2017 - Present)",
        },
        variants: [
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/82965",
            name: "Marvel Previews (2017)",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/82970",
            name: "Marvel Previews (2017)",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/82969",
            name: "Marvel Previews (2017)",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/74697",
            name: "Marvel Previews (2017)",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/72736",
            name: "Marvel Previews (2017)",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/75668",
            name: "Marvel Previews (2017)",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/65364",
            name: "Marvel Previews (2017)",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/65158",
            name: "Marvel Previews (2017)",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/65028",
            name: "Marvel Previews (2017)",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/75662",
            name: "Marvel Previews (2017)",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/74320",
            name: "Marvel Previews (2017)",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/73776",
            name: "Marvel Previews (2017)",
          },
        ],
        collections: [],
        collectedIssues: [],
        dates: [
          {
            type: "onsaleDate",
            date: "2099-10-30T00:00:00-0500",
          },
          {
            type: "focDate",
            date: "2019-10-07T00:00:00-0400",
          },
        ],
        prices: [
          {
            type: "printPrice",
            price: 0,
          },
        ],
        thumbnail: {
          path:
            "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available",
          extension: "jpg",
        },
        images: [],
        creators: {
          available: 1,
          collectionURI:
            "http://gateway.marvel.com/v1/public/comics/82967/creators",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/10021",
              name: "Jim Nausedas",
              role: "editor",
            },
          ],
          returned: 1,
        },
        characters: {
          available: 0,
          collectionURI:
            "http://gateway.marvel.com/v1/public/comics/82967/characters",
          items: [],
          returned: 0,
        },
        stories: {
          available: 2,
          collectionURI:
            "http://gateway.marvel.com/v1/public/comics/82967/stories",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/183698",
              name: "cover from Marvel Previews (2017)",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/183699",
              name: "story from Marvel Previews (2017)",
              type: "interiorStory",
            },
          ],
          returned: 2,
        },
        events: {
          available: 0,
          collectionURI:
            "http://gateway.marvel.com/v1/public/comics/82967/events",
          items: [],
          returned: 0,
        },
      },
    ],
  },
};

export const storiesResponse = {
  code: 200,
  status: "Ok",
  copyright: "© 2021 MARVEL",
  attributionText: "Data provided by Marvel. © 2021 MARVEL",
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2021 MARVEL</a>',
  etag: "d59f204a27f5eb33fa944e4b2c1a105c991671ad",
  data: {
    offset: 0,
    limit: 1,
    total: 113372,
    count: 1,
    results: [
      {
        id: 7,
        title:
          "Investigating the murder of a teenage girl, Cage suddenly learns that a three-way gang war is under way for control of the turf",
        description: "",
        resourceURI: "http://gateway.marvel.com/v1/public/stories/7",
        type: "story",
        modified: "1969-12-31T19:00:00-0500",
        thumbnail: null,
        creators: {
          available: 0,
          collectionURI:
            "http://gateway.marvel.com/v1/public/stories/7/creators",
          items: [],
          returned: 0,
        },
        characters: {
          available: 0,
          collectionURI:
            "http://gateway.marvel.com/v1/public/stories/7/characters",
          items: [],
          returned: 0,
        },
        series: {
          available: 1,
          collectionURI: "http://gateway.marvel.com/v1/public/stories/7/series",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/6",
              name: "Cage Vol. I (2002)",
            },
          ],
          returned: 1,
        },
        comics: {
          available: 1,
          collectionURI: "http://gateway.marvel.com/v1/public/stories/7/comics",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/941",
              name: "Cage Vol. I (Hardcover)",
            },
          ],
          returned: 1,
        },
        events: {
          available: 0,
          collectionURI: "http://gateway.marvel.com/v1/public/stories/7/events",
          items: [],
          returned: 0,
        },
        originalIssue: {
          resourceURI: "http://gateway.marvel.com/v1/public/comics/941",
          name: "Cage Vol. I (Hardcover)",
        },
      },
    ],
  },
};

export const comicsResponse = {
  code: 200,
  status: "Ok",
  copyright: "© 2021 MARVEL",
  attributionText: "Data provided by Marvel. © 2021 MARVEL",
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2021 MARVEL</a>',
  etag: "3a34429241f6e3b5e9e150478cc2233191c12bff",
  data: {
    offset: 0,
    limit: 1,
    total: 48604,
    count: 1,
    results: [
      {
        id: 82967,
        digitalId: 0,
        title: "Marvel Previews (2017)",
        issueNumber: 0,
        variantDescription: "",
        description: null,
        modified: "2019-11-07T08:46:15-0500",
        isbn: "",
        upc: "75960608839302811",
        diamondCode: "",
        ean: "",
        issn: "",
        format: "",
        pageCount: 112,
        textObjects: [],
        resourceURI: "http://gateway.marvel.com/v1/public/comics/82967",
        urls: [
          {
            type: "detail",
            url:
              "http://marvel.com/comics/issue/82967/marvel_previews_2017?utm_campaign=apiRef&utm_source=a6bc452661a7d43e08825c3f95534305",
          },
        ],
        series: {
          resourceURI: "http://gateway.marvel.com/v1/public/series/23665",
          name: "Marvel Previews (2017 - Present)",
        },
        variants: [
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/82965",
            name: "Marvel Previews (2017)",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/82970",
            name: "Marvel Previews (2017)",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/82969",
            name: "Marvel Previews (2017)",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/74697",
            name: "Marvel Previews (2017)",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/72736",
            name: "Marvel Previews (2017)",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/75668",
            name: "Marvel Previews (2017)",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/65364",
            name: "Marvel Previews (2017)",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/65158",
            name: "Marvel Previews (2017)",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/65028",
            name: "Marvel Previews (2017)",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/75662",
            name: "Marvel Previews (2017)",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/74320",
            name: "Marvel Previews (2017)",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/73776",
            name: "Marvel Previews (2017)",
          },
        ],
        collections: [],
        collectedIssues: [],
        dates: [
          {
            type: "onsaleDate",
            date: "2099-10-30T00:00:00-0500",
          },
          {
            type: "focDate",
            date: "2019-10-07T00:00:00-0400",
          },
        ],
        prices: [
          {
            type: "printPrice",
            price: 0,
          },
        ],
        thumbnail: {
          path:
            "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available",
          extension: "jpg",
        },
        images: [],
        creators: {
          available: 1,
          collectionURI:
            "http://gateway.marvel.com/v1/public/comics/82967/creators",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/10021",
              name: "Jim Nausedas",
              role: "editor",
            },
          ],
          returned: 1,
        },
        characters: {
          available: 0,
          collectionURI:
            "http://gateway.marvel.com/v1/public/comics/82967/characters",
          items: [],
          returned: 0,
        },
        stories: {
          available: 2,
          collectionURI:
            "http://gateway.marvel.com/v1/public/comics/82967/stories",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/183698",
              name: "cover from Marvel Previews (2017)",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/183699",
              name: "story from Marvel Previews (2017)",
              type: "interiorStory",
            },
          ],
          returned: 2,
        },
        events: {
          available: 0,
          collectionURI:
            "http://gateway.marvel.com/v1/public/comics/82967/events",
          items: [],
          returned: 0,
        },
      },
    ],
  },
};

export const storyDetails = {
  code: 200,
  status: "Ok",
  copyright: "© 2021 MARVEL",
  attributionText: "Data provided by Marvel. © 2021 MARVEL",
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2021 MARVEL</a>',
  etag: "683660ce7b4e9190b3f781a8e9a69260fee7500a",
  data: {
    offset: 0,
    limit: 20,
    total: 1,
    count: 1,
    results: [
      {
        id: 7,
        title:
          "Investigating the murder of a teenage girl, Cage suddenly learns that a three-way gang war is under way for control of the turf",
        description: "",
        resourceURI: "http://gateway.marvel.com/v1/public/stories/7",
        type: "story",
        modified: "1969-12-31T19:00:00-0500",
        thumbnail: null,
        creators: {
          available: 0,
          collectionURI:
            "http://gateway.marvel.com/v1/public/stories/7/creators",
          items: [],
          returned: 0,
        },
        characters: {
          available: 0,
          collectionURI:
            "http://gateway.marvel.com/v1/public/stories/7/characters",
          items: [],
          returned: 0,
        },
        series: {
          available: 1,
          collectionURI: "http://gateway.marvel.com/v1/public/stories/7/series",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/series/6",
              name: "Cage Vol. I (2002)",
            },
          ],
          returned: 1,
        },
        comics: {
          available: 1,
          collectionURI: "http://gateway.marvel.com/v1/public/stories/7/comics",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/941",
              name: "Cage Vol. I (Hardcover)",
            },
          ],
          returned: 1,
        },
        events: {
          available: 0,
          collectionURI: "http://gateway.marvel.com/v1/public/stories/7/events",
          items: [],
          returned: 0,
        },
        originalIssue: {
          resourceURI: "http://gateway.marvel.com/v1/public/comics/941",
          name: "Cage Vol. I (Hardcover)",
        },
      },
    ],
  },
};
