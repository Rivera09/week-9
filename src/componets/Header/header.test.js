import React from "react";
import ReactDOM from "react-dom";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom";

import Header from "./index";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<Header />, div);
});

it("renders header", () => {
  const { getByTestId } = render(<Header title="Test" />);
  expect(getByTestId("headerTitle")).toHaveTextContent("Test");
});
