import React from "react";

const Header: React.FC<{ title: string; image?: string | null }> = ({
  title,
  image,
}) => {
  return (
    <header>
      <img
        src={
          image ||
          "http://x.annihil.us/u/prod/marvel/i/mg/3/40/4bb4680432f73/landscape_incredible.jpg"
        }
        alt="header"
      />
      <h1 data-testid="headerTitle">{title}</h1>
    </header>
  );
};

export default Header;
