import React, { ChangeEventHandler, FormEventHandler } from "react";

import { CharactersFilterFormProps } from "../../consts";

const CharactersFilterForm: React.FC<CharactersFilterFormProps> = ({
  onChange,
  onSubmit,
  comics,
  stories,
}) => (
  <form
    className="filter-form"
    onSubmit={onSubmit as FormEventHandler<HTMLFormElement>}
  >
    <div className="form-element">
      <label>Character name</label>
      <input
        type="text"
        placeholder="character name"
        name="characterName"
        onChange={onChange as ChangeEventHandler<HTMLInputElement>}
        autoComplete="off"
      />
    </div>
    <div className="form-element comic-filter-select">
      <label>Comic</label>
      <select
        name="comicId"
        onChange={onChange as ChangeEventHandler<HTMLSelectElement>}
      >
        <option value="">Select a comic</option>
        {comics?.map((comic) => (
          <option value={comic.id} key={comic.id}>
            {comic.title}
          </option>
        ))}
      </select>
    </div>
    <div className="form-element story-filter-select">
      <label>Story</label>
      <select name="storyId" onChange={(e) => onChange(e)}>
        <option value="">Select a story</option>
        {stories?.map((story) => (
          <option value={story.id} key={story.id}>
            {story.title}
          </option>
        ))}
      </select>
    </div>

    <button>Buscar</button>
  </form>
);
export default CharactersFilterForm;
