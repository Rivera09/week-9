import React from "react";
import ReactDOM from "react-dom";

import CharactersFilterForm from "./index";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<CharactersFilterForm />, div);
});