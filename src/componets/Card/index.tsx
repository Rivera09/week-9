import React from "react";
import { Link } from "react-router-dom";

import { connect } from "react-redux";
import { saveBookmark, removeBookmark } from "../../actions";

const Card: React.FC<{
  cardTitle?: string;
  cardCover?: string | null;
  saveBookmark: Function;
  removeBookmark: Function;
  id?: string | number;
  bookmarkedElements: { id: string; cardCover: string; coverTitle: string }[];
  linkTo?: string;
  type?: string;
}> = ({
  cardTitle,
  cardCover,
  saveBookmark,
  removeBookmark,
  id,
  bookmarkedElements,
  linkTo,
  type,
}) => {
  const bookmarkedIds = bookmarkedElements.map((bookmark) => bookmark.id);
  return (
    <div className="card">
      <Link to={linkTo || "/"} className="black-text">
        <img
          src={
            cardCover ||
            "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available/portrait_xlarge.jpg"
          }
          alt="card"
        />
      </Link>
      <div className="card-info">
        <p>{cardTitle || "placeholder"}</p>
        {bookmarkedIds.includes(id as string) ? (
          <button
            className="bookmarkBtn"
            onClick={() => removeBookmark(id || "")}
          >
            <i className="fas fa-bookmark"></i>
          </button>
        ) : (
          <button
            className="bookmarkBtn"
            onClick={() => saveBookmark({ id, cardCover, cardTitle, type })}
            data-testid={`bookmarkTestBtn-${id}`}
          >
            <i className="far fa-bookmark"></i>
          </button>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = (state: any) => ({
  bookmarkedElements: state?.bookmarks,
});

export default connect(mapStateToProps, { saveBookmark, removeBookmark })(Card);
