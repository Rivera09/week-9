import React from "react";

import { ComicsFilterFormProps } from "../../consts";

const ComicsFilterForm: React.FC<ComicsFilterFormProps> = ({
  onChange,
  onSubmit,
}) => {
  return (
    <form className="filter-form" onSubmit={(e) => onSubmit(e)}>
      <div className="form-element">
        <label>Comic name</label>
        <input
          type="text"
          autoComplete="off"
          name="comicName"
          placeholder="Comic name"
          onChange={(e) => onChange(e)}
        />
      </div>
      <div className="form-element">
        <label>format</label>

        <select name="comicFormatType" onChange={(e) => onChange(e)}>
          <option value="">Select format</option>
          <option value="comic">Comic</option>
          <option value="collection">Collection</option>
        </select>
      </div>

      <button type="submit">Buscar</button>
    </form>
  );
};

export default ComicsFilterForm;
