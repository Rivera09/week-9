import React from "react";
import ReactDOM from "react-dom";

import ComicsFilterForm from "./index";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<ComicsFilterForm />, div);
});