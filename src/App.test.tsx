import React from "react";
import {
  render,
  screen,
  waitForDomChange,
  fireEvent,
} from "@testing-library/react";
import { rest } from "msw";
import { setupServer } from "msw/node";
import "@testing-library/jest-dom";

import App from "./App";
import { charactersResponse } from "./mockedData";

const server = setupServer(
  rest.get(
    "https://gateway.marvel.com/v1/public/characters",
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(charactersResponse));
    }
  )
);

beforeAll(() => server.listen());
afterAll(() => server.close());
afterEach(() => server.resetHandlers());

test("renders learn react link", () => {
  render(<App />);
  const linkElement = screen.getByText(/Home/i);
  expect(linkElement).toBeInTheDocument();
});

it("Bookmark works", async () => {
  const { getByText, getByTestId } = render(<App />);

  await waitForDomChange();
  const bookmarkTest = getByTestId("bookmarkTestBtn-1011334");
  const bookmarksBtn = getByText("Bookmarks");
  fireEvent.click(bookmarkTest);
  fireEvent.click(bookmarksBtn);
  await waitForDomChange();
  getByText("3-D Man");
});
