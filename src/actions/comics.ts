import { LOAD_COMICS } from "./types";

import Connection from "../apiConnection";
import {
  COMICS_ENDPOINT_URL,
  COMICS_NAME_ENDPOINT_URL,
  COMICS_FORMAT_ENDPOINT_URL,
  COMIC_DETAILS_ENDPOINT_URL,
} from "../consts";

const apiConnection: any = new Connection();

export const fetchComics = (
  limit: number,
  filterType: "name" | "format" | null,
  filterData: string | null
) => async (dispatch: any) => {
  let url: string;
  switch (filterType) {
    case "name":
      url = COMICS_NAME_ENDPOINT_URL.replace(":name", filterData || "").replace(
        ":limit",
        limit.toString()
      );
      break;
    case "format":
      url = COMICS_FORMAT_ENDPOINT_URL.replace(
        ":type",
        filterData || ""
      ).replace(":limit", limit.toString());
      break;
    default:
      url = COMICS_ENDPOINT_URL.replace(":limit", limit.toString());
      break;
  }
  const { resBody, error } = await apiConnection.getData(url);
  if (!error) dispatch({ type: LOAD_COMICS, payload: resBody.data });
};

export const fetchComicData = (id: string) => async (dispatch: any) => {
  const { resBody, error } = await apiConnection.getData(
    COMIC_DETAILS_ENDPOINT_URL.replace(":id", id)
  );
  if (!error) dispatch({ type: LOAD_COMICS, payload: resBody.data });
};
