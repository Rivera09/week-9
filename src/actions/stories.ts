import { LOAD_STORIES } from "./types";

import Connection from "../apiConnection";
import { STORIES_ENDPOINT_URL, STORY_DATA_ENDPOINT_URL } from "../consts";

const apiConnection: any = new Connection();

export const fetchStories = (limit: number) => async (dispatch: any) => {
  const { resBody, error } = await apiConnection.getData(
    STORIES_ENDPOINT_URL.replace(":limit", limit.toString())
  );
  if (!error) dispatch({ type: LOAD_STORIES, payload: resBody.data });
};

export const fetchStoryDetails = (id: string) => async (dispatch: any) => {
  const { resBody, error } = await apiConnection.getData(
    STORY_DATA_ENDPOINT_URL.replace(":id", id)
  );

  if (!error) dispatch({ type: LOAD_STORIES, payload: resBody.data });
};
