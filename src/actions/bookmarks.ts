import { BOOKMARK_ELEMENT, UBBOOKMARK_ELEMENT } from "./types";

export const saveBookmark = (element: any) => (dispatch: any) => {
  dispatch({ type: BOOKMARK_ELEMENT, payload: element });
};

export const removeBookmark = (id: string) => (dispatch: any) => {
  dispatch({ type: UBBOOKMARK_ELEMENT, payload: id });
};
