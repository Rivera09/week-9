import { LOAD_CHARACTERS, LOAD_CHARACTER_DATA } from "./types";

import Connection from "../apiConnection";
import {
  CHARACTERS_ENDPOINT_URL,
  CHARACTER_DETAILS_ENDPOINT_URL,
  CHARACTERS_NAME_FILTER_ENDPOINT_URL,
  CHARACTERS_COMIC_FILTER_ENDPOINT_URL,
  CHARACTERS_STORY_FILTER_ENDPOINT_URL,
} from "../consts";

const apiConnection: any = new Connection();

export const fetchCharacters = (
  limit: number,
  filterType: "name" | "comic" | "story" | null,
  filterData: string | null
) => async (dispatch: any) => {
  let url: string;
  switch (filterType) {
    case "name":
      url = CHARACTERS_NAME_FILTER_ENDPOINT_URL.replace(
        ":limit",
        limit.toString()
      ).replace(":name", filterData ? filterData : "");
      break;
    case "comic":
      url = CHARACTERS_COMIC_FILTER_ENDPOINT_URL.replace(
        ":comicId",
        filterData ? filterData : ""
      );
      break;
    case "story":
      url = CHARACTERS_STORY_FILTER_ENDPOINT_URL.replace(
        ":storyId",
        filterData ? filterData : ""
      );

      break;
    default:
      url = CHARACTERS_ENDPOINT_URL.replace(":limit", limit.toString());
  }
  const { resBody } = await apiConnection.getData(url);
  dispatch({ type: LOAD_CHARACTERS, payload: resBody.data });
};

export const fetchCharacterData = (id: string) => async (dispatch: any) => {
  const { resBody } = await apiConnection.getData(
    CHARACTER_DETAILS_ENDPOINT_URL.replace(":id", id)
  );
  dispatch({ type: LOAD_CHARACTER_DATA, payload: resBody.data });
};
